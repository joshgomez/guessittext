﻿namespace GuessItText.Models
{
    public class CatJsonResponse
    {
        public string Fact { get; set; }
        public int Length { get; set; }
    }
}
