﻿namespace GuessItText.Models
{
    public class RandomJsonResponse
    {
        public RandomJsonResponse(RandomType type, string value)
        {
            this._type = type;
            this.Value = value;
        }

        public enum RandomType
        {
            Cat = 1,
            ChuckNorris = 2,
            DonaldTrump = 3
        }

        private RandomType _type;
        public int Type
        {
            get { return (int)_type; }
            set { _type = (RandomType)value; }
        }

        public string Value { get; set; }
    }
}
