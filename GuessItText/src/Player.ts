﻿
export default class Player {
    public score: number = 0;
    public countCorrect: number = 0;
    public countWrong: number = 0;

    public addScore(value: number): void {
        this.score += value;
        if (this.score < 0) {
            this.score = 0;
        }
    }

    public addCorrect(): void {
        this.countCorrect += 1;
    }

    public addWrong(): void {
        this.countWrong += 1;
    }

    public guessed(): number {
        return this.countCorrect + this.countWrong;
    }

    public correctPercent(): string {

        if (this.guessed() == 0) {
            return (0).toString();
        }

        return ((this.countCorrect / this.guessed()) * 100.0).toFixed(1);
    }
}