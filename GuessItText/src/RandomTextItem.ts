

export default class RandomTextItem {
    public id: number = 0;
    public content: string = "";
    public type: number = 0;
    public variant: string = "warning"
    public createdAt!: Date;
    public score: number = 0;

    constructor(id: number, content: string) {
        this.id = id;
        this.content = content;
        this.createdAt = new Date();
    }

    public ContentInArray(): Array<string> {
        return [this.content]
    }

    public GetTimeLapseInSecond(): number {
        const diff = new Date().getTime() - this.createdAt.getTime();
        return diff / 1000;
    }

    public GetPositiveScore(): number {

        const normalize = 1.2; //Give the player time to get full points.
        let score = Math.round(this.content.length / this.GetTimeLapseInSecond() - normalize);
        if (score > this.content.length) {
            score = this.content.length;
        }

        return score;
    }

    public GetNegativeScore(durationInSeconds: number): number {
        return -Math.round(this.content.length / (durationInSeconds - this.GetTimeLapseInSecond()));
    }
}