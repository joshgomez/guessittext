import Vue from 'vue';
import 'bootstrap';
import '../node_modules/vue-typed-js/dist/vue-typed-js.browser.js';
import BootstrapVue from 'bootstrap-vue';
var VueTypedJs = require('vue-typed-js');
import App from './App.vue';

Vue.config.productionTip = true;
Vue.use(BootstrapVue);
Vue.use(VueTypedJs);

new Vue({
    render: h => h(App)
}).$mount('#app');