﻿using GuessItText.Models;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuessItText.Repositories
{
    public class RandonRepository
    {
        private static readonly Random _random = new Random();

        public RandonRepository(IHttpClientFactory factory)
        {
            _factory = factory;
        }

        private readonly IHttpClientFactory _factory;

        /// <summary>
        /// Get a random text from all registered sources.
        /// </summary>
        /// <returns></returns>
        public async Task<RandomJsonResponse> GetRandomAsync()
        {
            int maxValue = (int)Enum.GetValues(typeof(RandomJsonResponse.RandomType)).Cast<RandomJsonResponse.RandomType>().Last();
            int randomTypeNum = _random.Next(1, maxValue + 1);
            var randomType = (RandomJsonResponse.RandomType)randomTypeNum;
            switch (randomType)
            {
                case RandomJsonResponse.RandomType.Cat:
                    return await GetRandomCatAsync();
                case RandomJsonResponse.RandomType.ChuckNorris:
                    return await GetRandomChuckNorrisAsync();
                case RandomJsonResponse.RandomType.DonaldTrump:
                    return await GetRandomDonaldTrumpAsync();
                default:
                    return null;
            }
        }

        public async Task<RandomJsonResponse> GetRandomCatAsync()
        {
            using (var client = _factory.CreateClient())
            {
                string JsonStr = await client.GetStringAsync("https://catfact.ninja/fact");
                var ret = JsonConvert.DeserializeObject<CatJsonResponse>(JsonStr);
                return new RandomJsonResponse(RandomJsonResponse.RandomType.Cat, ret.Fact);
            }
        }

        public async Task<RandomJsonResponse> GetRandomChuckNorrisAsync()
        {
            using (var client = _factory.CreateClient())
            {
                string JsonStr = await client.GetStringAsync("https://api.chucknorris.io/jokes/random");
                var ret = JsonConvert.DeserializeObject<ChuckNorrisJsonResponse>(JsonStr);
                return new RandomJsonResponse(RandomJsonResponse.RandomType.ChuckNorris, ret.Value);
            }
        }

        public async Task<RandomJsonResponse> GetRandomDonaldTrumpAsync()
        {
            using (var client = _factory.CreateClient())
            {
                string JsonStr = await client.GetStringAsync("https://api.tronalddump.io/random/quote");
                var ret = JsonConvert.DeserializeObject<DonaldTrumpJsonResponse>(JsonStr);
                return new RandomJsonResponse(RandomJsonResponse.RandomType.DonaldTrump, ret.Value);
            }
        }


        
    }
}
