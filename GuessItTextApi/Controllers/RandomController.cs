﻿using GuessItText.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace GuessItTextApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RandomController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public RandomController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        [HttpGet("Any")]
        public async Task<IActionResult> GetAny()
        {
            var repo = new RandonRepository(_httpClientFactory);
            var ret = await repo.GetRandomAsync();
            return Ok(ret);
        }
    }
}
